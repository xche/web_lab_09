/**
 * Created by xingjianche on 16/04/2017.
 */
function displayTime() {
    var date = new Date();
    document.getElementById('clock').innerHTML = date.toLocaleTimeString();
}


var interval = setInterval(displayTime, 1000);

function stop() {
    clearInterval(interval);
}

function resume() {
    interval = setInterval(displayTime, 1000);
}