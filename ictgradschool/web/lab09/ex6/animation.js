/**
 * Created by xingjianche on 17/04/2017.
 */
var page = 0;

function animation() {
    document.getElementById('page' + page).classList.add("pageAnimation");
    page += 1;
    document.getElementById('page' + page).addEventListener('animationend', end);
}

function end() {
    document.getElementById('page' + page).style.zIndex = 1;
}